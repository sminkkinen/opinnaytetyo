-- create a scene object
local scene = storyboard.newScene()

local object1
local object2
local object3

local wallet
walletStatus = wallet

-- funtion called when the scene's view does not exist

function scene:createScene(inEvent)

	print("gameScene", "createScene()")

	-- adding background
	local bgstore = display.newImage("bgstore.png")
	bgstore.x = display.contentWidth / 2
	bgstore.y = display.contentHeight / 2
	bgstore.xScale = 2
	bgstore.yScale = 2
	self.view:insert(bgstore) --self.view is the DisplayGroup created by storyboard
	
	-- adding categories
	local group1 = display.newImage("group1.png")
	group1.x = display.contentCenterX - 350
	group1.y = display.contentCenterY - 180
	self.view:insert(group1) 
	
	if group == 1 then
		wallet = math.random(2,10)
		local group2grey = display.newImage("group2grey.png")
		group2grey.x = display.contentCenterX - 250
		group2grey.y = display.contentCenterY - 180
		self.view:insert(group2grey) 
		local group3grey = display.newImage("group3grey.png")
		group3grey.x = display.contentCenterX - 150
		group3grey.y = display.contentCenterY - 180
		self.view:insert(group3grey)
		else
			if group == 2 then
				wallet = math.random(10,20)			
				local group2 = display.newImage("group2.png")
				group2.x = display.contentCenterX - 250
				group2.y = display.contentCenterY - 180
				self.view:insert(group2)
				local group3grey = display.newImage("group3grey.png")
				group3grey.x = display.contentCenterX - 150
				group3grey.y = display.contentCenterY - 180
				self.view:insert(group3grey)
				else
					if group == 3 then
						wallet = math.random(20,30)
						local group2 = display.newImage("group2.png")
						group2.x = display.contentCenterX - 250
						group2.y = display.contentCenterY - 180
						self.view:insert(group2)
						local group3 = display.newImage("group3.png")
						group3.x = display.contentCenterX - 150
						group3.y = display.contentCenterY - 180
						self.view:insert(group3)
					end
			end
	end
		
-- function readJsonFile() -- Zammetti location 1804 and 6033

	-- local path = system.pathForFile("products.json", system.DocumentsDirectory)
	-- local products
	-- if path ~= nil then   
		-- local filehandle = io.open(path, "r") -- io.open accepts the path and mode "r" which is for reading
		-- if filehandle == nil then
			-- print ei tiedostoja
		-- else
			-- products = json.decode(filehandle:read("*a"))
		-- io.close(filehandle)
		-- end
		-- return products
	-- end
-- end
	
	local ground = display.newImage("floor.png")
	ground.x = display.contentCenterX + 200
	ground.y = display.contentCenterY + 200
	self.view:insert(ground) 
	
	local target = display.newImage("target.png")
	target.x = display.contentCenterX + 200
	target.y = display.contentCenterY + 100
	self.view:insert(target) 
	
	local object1 = display.newImage("object1.png")
	object1.x = display.contentCenterX - 350
	object1.y = display.contentCenterY - 50
	object1:addEventListener("touch", 
		function(inEvent)
		
			if inEvent.phase == "began" then
				object1.x = display.contentCenterX - 350
				object1.y = display.contentCenterY - 50
			elseif inEvent.phase == "moved" then
				object1.x = display.contentCenterX + 200
				object1.y = display.contentCenterY + 100
				walletStatus = wallet-Price1
				print ("wallet-Price1")
			end
		
		end)
	self.view:insert(object1) 
	
	local object2 = display.newImage("object2.png")
	object2.x = display.contentCenterX - 250
	object2.y = display.contentCenterY - 50
	object2:addEventListener("touch", 
		function(inEvent)
		
			if inEvent.phase == "began" then
				object2.x = display.contentCenterX - 250
				object2.y = display.contentCenterY - 50
			elseif inEvent.phase == "moved" then
				object2.x = display.contentCenterX + 200
				object2.y = display.contentCenterY + 100
				walletStatus = wallet-Price2
				print ("wallet-Price2")
			end
		
		end)
	self.view:insert(object2) 
	
	local object3 = display.newImage("object3.png")
	object3.x = display.contentCenterX - 150
	object3.y = display.contentCenterY - 50
	object3:addEventListener("touch", 
		function(inEvent)
		
			if inEvent.phase == "began" then
				object3.x = display.contentCenterX - 150
				object3.y = display.contentCenterY - 50
			elseif inEvent.phase == "moved" then
				object3.x = display.contentCenterX + 200
				object3.y = display.contentCenterY + 100
				walletStatus = wallet-Price3
				print ("wallet-Price3")
			end
		
		end)
	self.view:insert(object3) 
	
	Price1 = 2
	local txtPrice1 = display.newText("2,00", 0,0, Verdana, 20)
	txtPrice1:setTextColor(0,0,0)
	txtPrice1.x = display.contentCenterX - 350
	txtPrice1.y = display.contentCenterY + 10
	self.view:insert(txtPrice1) 
	
	Price2 = 3
	local txtPrice2 = display.newText("3,00", 0,0, Verdana, 20)
	txtPrice2:setTextColor(0,0,0)
	txtPrice2.x = display.contentCenterX - 250
	txtPrice2.y = display.contentCenterY + 10	
	self.view:insert(txtPrice2) 
	
	Price3 = 4
	local txtPrice3 = display.newText("4,00", 0,0, Verdana, 20)
	txtPrice3:setTextColor(0,0,0)
	txtPrice3.x = display.contentCenterX - 150
	txtPrice3.y = display.contentCenterY + 10	
	self.view:insert(txtPrice3) 
	
	local txtWallet = display.newText("Rahaa lompakossa: " .. wallet, 0,0, Verdana, 20)
	txtWallet:setTextColor(0,0,0)
	txtWallet.x = display.contentCenterX + 200
	txtWallet.y = display.contentCenterY - 100	
	self.view:insert(txtWallet) 
		
	local nextBtn = display.newImage("next.png")
	nextBtn.x = display.contentCenterX + 370
	nextBtn.y = display.contentCenterY + 200
	nextBtn.name = "nextbutton"
	nextBtn:addEventListener("touch", 
		function(inEvent)
			if inEvent.phase == "ended" then
				print("menuScene", "next-button tapped")
				storyboard.gotoScene("checkoutScene", "slideRight", 500)
			end
		end)
	self.view:insert(nextBtn)
	
end -- end create scene

-- function called before scene has moved on screen

function scene:willEnterScene(inEvent)
	
	print("gameScene", "WillEnterScene()")
	
end -- ending WillEnterScene

-- function called after scene has moved on screen

function scene:enterScene(inEvent)
	
	print("gameScene", "enterScene()")
	
end -- ending enterScene

-- function called before scene moves off the screen

function scene:exitScene(inEvent)

	print("gameScene", "exitScene()")
		
end --ending exitScene

-- function called after scene has moved off screen

function scene:didExitScene(inEvent)

	print("gameScene", "didExitScene()")
	
end -- endin didExitScene

-- function called removing scene

function scene:destroyScene(inEvent)

	print("gameScene", "destroyScene()")
	
	-- clear up
	self.view:removeSelf()
	self.view = nil
	
end -- ending destroyScene

-- execution

print("gameScene", "Beginning execution");

-- Add scene lifecycle event handlers.
scene:addEventListener("createScene", scene);
scene:addEventListener("willEnterScene", scene);
scene:addEventListener("enterScene", scene);
scene:addEventListener("exitScene", scene);
scene:addEventListener("didExitScene", scene);
scene:addEventListener("destroyScene", scene);

return scene;