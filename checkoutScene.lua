-- create a scene object
local scene = storyboard.newScene()


-- funtion called when the scene's view does not exist

function scene:createScene(inEvent)

	print("checkoutScene", "createScene()")

	-- adding objects
	
	local checkoutbground = display.newImage("checkoutbground.png")
	checkoutbground.x = display.contentWidth / 2
	checkoutbground.y = display.contentHeight / 2
	checkoutbground.xScale = 2
	checkoutbground.yScale = 2
	self.view:insert(checkoutbground)
	
	local ground = display.newImage("floor.png")
	ground.x = display.contentCenterX - 200
	ground.y = display.contentCenterY + 50
	self.view:insert(ground) 
	
	local target = display.newImage("target.png")
	target.x = display.contentCenterX - 200
	target.y = display.contentCenterY - 50
	self.view:insert(target) 
		
	local txtWalletStatus = display.newText("Rahaa lompakossa: " .. walletStatus, 0,0, Verdana, 20)
	txtWalletStatus:setTextColor(0,0,0)
	txtWalletStatus.x = display.contentCenterX + 100
	txtWalletStatus.y = display.contentCenterY - 100	
	self.view:insert(txtWalletStatus) 		
	
	if walletStatus >= 0 then
		local txtContinue = display.newText("Seuraava taso", 0,0, Verdana, 40)
		txtContinue:setTextColor(255,255,255)
		txtContinue.x = display.contentCenterX + 100
		txtContinue.y = display.contentCenterY + 40
		txtContinue:addEventListener("touch", 
		function(inEvent)
			if inEvent.phase == "ended" then
				loadGameData()
				group = group + 1
				print("checkoutScene", "Seuraava taso tapped")
				storyboard.gotoScene("gameScene", "slideRight", 500)
			end
		end)
		self.view:insert(txtContinue)
		
		local txtQuit = display.newText("Poistu", 0,0, Verdana, 40)
		txtQuit:setTextColor(255,255,255)
		txtQuit.x = display.contentCenterX + 100
		txtQuit.y = display.contentCenterY + 110
		txtQuit:addEventListener("touch", 
			function(inEvent)
				if inEvent.phase == "ended" then
					print("checkoutScene", "Poistu tapped")
					-- transition.to method allows to change (animate) attributes of an object over time
					-- object to be animated (scene.view) and two attributes alpha and time, alpha meaning 
					-- the target value and time in milliseconds of animation to take complete
					transition.to(scene.view, {alpha = 0, time = 500, 
					onComplete = function()
						os.exit()
					end})
				end
			end)
			self.view:insert(txtQuit)
		else
			if walletStatus < 0 then
				local txtAgain = display.newText("Yritä uudelleen", 0,0, Verdana, 40)
				txtAgain:setTextColor(255,255,255)
				txtAgain.x = display.contentCenterX + 100
				txtAgain.y = display.contentCenterY + 40
				txtAgain:addEventListener("touch", 
				function(inEvent)
					if inEvent.phase == "ended" then
						loadGameData()
						group = group
						print("checkoutScene", "Yritä uudelleen tapped")
						storyboard.gotoScene("gameScene", "slideRight", 500)
					end
				end)
				self.view:insert(txtAgain)
			end	
		local txtQuit = display.newText("Poistu", 0,0, Verdana, 40)
		txtQuit:setTextColor(255,255,255)
		txtQuit.x = display.contentCenterX + 100
		txtQuit.y = display.contentCenterY + 110
		txtQuit:addEventListener("touch", 
			function(inEvent)
				if inEvent.phase == "ended" then
					print("checkoutScene", "Poistu tapped")
					-- transition.to method allows to change (animate) attributes of an object over time
					-- object to be animated (scene.view) and two attributes alpha and time, alpha meaning 
					-- the target value and time in milliseconds of animation to take complete
					transition.to(scene.view, {alpha = 0, time = 500, 
					onComplete = function()
						os.exit()
					end})
				end
			end)
		self.view:insert(txtQuit) 
	end		
		
end -- end create scene

-- function called before scene has moved on screen

function scene:willEnterScene(inEvent)
	
	print("gameScene", "WillEnterScene()")
	
end -- ending WillEnterScene

-- function called after scene has moved on screen

function scene:enterScene(inEvent)
	
	print("gameScene", "enterScene()")
	
end -- ending enterScene

-- function called before scene moves off the screen

function scene:exitScene(inEvent)

	print("gameScene", "exitScene()")
	
end --ending exitScene

-- function called after scene has moved off screen

function scene:didExitScene(inEvent)

	print("gameScene", "didExitScene()")
	
end -- endin didExitScene

-- function called removing scene

function scene:destroyScene(inEvent)

	print("gameScene", "destroyScene()")
	
	-- clear up
	self.view:removeSelf()
	self.view = nil
	
end -- ending destroyScene

-- execution

print("gameScene", "Beginning execution");

-- Add scene lifecycle event handlers.
scene:addEventListener("createScene", scene);
scene:addEventListener("willEnterScene", scene);
scene:addEventListener("enterScene", scene);
scene:addEventListener("exitScene", scene);
scene:addEventListener("didExitScene", scene);
scene:addEventListener("destroyScene", scene);

return scene;